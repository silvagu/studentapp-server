require('dotenv').config();
import { connect, connection } from "mongoose";

const clear = async () => {
  try {
    console.log('[clear] : running...');

    await connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
    await connection.db.dropCollection('students');

    console.log("[clear] : success");
  } catch (e) {
    throw new Error("failed to clear database. Error: " + e.message);
  } finally {
    await connection.close();
  }
}

clear();
