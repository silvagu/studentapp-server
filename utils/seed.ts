import { StudentSchema } from "../src/students/students.schema";

require('dotenv').config();
import { connect, model, connection } from "mongoose";
import * as students from "./students.json";
const seed = async () => {
  try {
    console.log('[seed] : running...');

    await connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
    const studentModel = model("Student", StudentSchema);

    await studentModel.insertMany(students);

    console.log("[seed] : success");
  } catch (e) {
    throw new Error(`failed to seed database. Error: ${e.message}`);
  } finally {
    await connection.close();
  }
}

seed();
