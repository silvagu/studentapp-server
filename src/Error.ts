import { ApiProperty } from '@nestjs/swagger';

export class ApiError {
  @ApiProperty()
  ok: false;

  @ApiProperty()
  message: string;
}
