import { Document, Model, model, Schema } from 'mongoose';

export const StudentSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  surname: {
    type: String,
    required: true,
  },
  patronymic: String,
});

StudentSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function(doc, ret) {
    delete ret._id;
  },
});

export interface StudentModel extends Document {
  name: string;
  surname: string;
  patronymic: string;
}

const Student: Model<StudentModel> = model<StudentModel>(
  'Student',
  StudentSchema,
);

export default Student;
