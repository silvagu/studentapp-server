import { ApiProperty } from '@nestjs/swagger';

export class StudentDto {
  @ApiProperty()
  readonly id?: string;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly surname: string;

  @ApiProperty()
  readonly patronymic: string;
}

export class RequestStudentDto {
  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly surname: string;

  @ApiProperty()
  readonly patronymic: string;
}
