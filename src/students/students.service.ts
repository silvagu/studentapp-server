import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StudentModel } from './students.schema';
import { RequestStudentDto, StudentDto } from './students.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectModel('Student') private studentModel: Model<StudentModel>,
  ) {}

  async findAllStudents(): Promise<StudentDto[]> {
    return this.studentModel.find({});
  }

  async create(studentDto: RequestStudentDto): Promise<StudentDto> {
    return this.studentModel.create(studentDto);
  }

  async update(id: string, studentDto: RequestStudentDto): Promise<StudentDto> {
    return this.studentModel.findByIdAndUpdate(
      id,
      { $set: studentDto },
      { useFindAndModify: false, returnOriginal: false },
    );
  }

  async delete(id: string): Promise<{ message: string }> {
    await this.studentModel.findByIdAndDelete(id);
    return { message: 'User has been deleted.' };
  }
}
