import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiDefaultResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { StudentsService } from './students.service';
import { RequestStudentDto, StudentDto } from './students.dto';
import { ApiError } from '../Error';

@ApiTags('Students')
@Controller('students')
export class StudentsController {
  constructor(private studentsService: StudentsService) {}

  @Get()
  @ApiOperation({ description: "Find all the student's records" })
  @ApiOkResponse({
    description: 'Retrieve student document(s)',
    type: [StudentDto],
  })
  @ApiNotFoundResponse({ description: 'Not found', type: ApiError })
  @ApiDefaultResponse({ description: 'unexpected error', type: ApiError })
  async all(): Promise<StudentDto[]> {
    return this.studentsService.findAllStudents();
  }

  @Post()
  @ApiOperation({ description: 'Create a new student record' })
  @ApiCreatedResponse({
    description: 'Retrieve student document(s)',
    type: StudentDto,
  })
  @ApiForbiddenResponse({ description: 'Forbidden', type: ApiError })
  @ApiNotFoundResponse({ description: 'Not found', type: ApiError })
  @ApiDefaultResponse({ description: 'unexpected error', type: ApiError })
  async create(@Body() studentDto: RequestStudentDto): Promise<StudentDto> {
    return this.studentsService.create(studentDto);
  }

  @Put(':id')
  @ApiOperation({ description: 'Update a single student record' })
  @ApiOkResponse({
    description: 'Retrieve student document(s)',
    type: StudentDto,
  })
  @ApiForbiddenResponse({ description: 'Forbidden', type: ApiError })
  @ApiNotFoundResponse({ description: 'Not found', type: ApiError })
  @ApiDefaultResponse({ description: 'unexpected error', type: ApiError })
  update(
    @Param('id') id: string,
    @Body() studentDto: RequestStudentDto,
  ): Promise<StudentDto> {
    return this.studentsService.update(id, studentDto);
  }

  @Delete(':id')
  @ApiOperation({ description: 'Delete a single student record.' })
  @ApiOkResponse({
    description: 'deletes a single student based on the ID.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden', type: ApiError })
  @ApiNotFoundResponse({ description: 'Not found', type: ApiError })
  @ApiDefaultResponse({ description: 'unexpected error', type: ApiError })
  delete(@Param('id') id: string): Promise<{ message: string }> {
    return this.studentsService.delete(id);
  }
}
