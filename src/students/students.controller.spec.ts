import * as request from 'supertest';
import * as students from '../../utils/students.json';
import { Test, TestingModule } from '@nestjs/testing';
import { StudentsController } from './students.controller';
import { StudentsService } from './students.service';
import { getModelToken } from '@nestjs/mongoose';
import { HttpStatus, INestApplication } from '@nestjs/common';

const student = {
  _id: '5f51126ee6a98f7b424edd51',
  name: 'Степан Романович Буров',
  surname: 'Романович',
  patronymic: 'Буров',
};
const create = jest.fn();
const findByIdAndUpdate = jest.fn();
const findByIdAndDelete = jest.fn();
const mockStudentModel = {
  find: () => Promise.resolve(students),
  save: () => Promise.resolve(student),
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
};

const mockMongooseTokens = [
  {
    provide: getModelToken('Student'),
    useValue: mockStudentModel,
  },
];

describe('StudentController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentsController],
      providers: [StudentsService, ...mockMongooseTokens],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  describe('students endpoint', () => {
    test('should return all the students', async () => {
      return request(app.getHttpServer())
        .get('/students')
        .set('Accept', 'application/json')
        .expect(HttpStatus.OK)
        .expect(({ body }) => {
          expect(body.length).toEqual(5);
        });
    });
    test('should return the created student', async () => {
      create.mockReturnValue(Promise.resolve(student));
      return request(app.getHttpServer())
        .post('/students')
        .set('Accept', 'application/json')
        .send({
          name: 'Степан Романович Буров',
          surname: 'Романович',
          patronymic: 'Буров',
        })
        .expect(HttpStatus.CREATED)
        .expect(({ body }) => {
          expect(body).toEqual(student);
        });
    });
    test('should return the updated student', async () => {
      const updatedStudent = { ...student, name: 'Ярослав' };
      findByIdAndUpdate.mockReturnValue(Promise.resolve(updatedStudent));
      return request(app.getHttpServer())
        .put('/students/5f51126ee6a98f7b424edd51')
        .set('Accept', 'application/json')
        .send({
          name: 'Ярослав',
        })
        .expect(HttpStatus.OK)
        .expect(({ body }) => {
          expect(body).toEqual(updatedStudent);
        });
    });
    test('should delete a student', async () => {
      findByIdAndDelete.mockReturnValue(Promise.resolve(''));
      return request(app.getHttpServer())
        .del('/students/5f51126ee6a98f7b424edd51')
        .set('Accept', 'application/json')
        .expect(HttpStatus.OK)
        .expect(({ body }) => {
          expect(body.message).toEqual('User has been deleted.');
        });
    });
  });
});
