import { Module } from '@nestjs/common';
import { StudentsModule } from './students/students.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [StudentsModule, MongooseModule.forRoot(process.env.MONGO_URL)],
})
export class AppModule {}
